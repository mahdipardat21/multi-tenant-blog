import { AppModule } from './app.module';
import { NestFactory } from '@nestjs/core';


const bootstrap = async (port: number) => {

    const app = await NestFactory.create(AppModule);
    app.listen(port);
    
}

const port: number = parseInt(process.env.PORT) || 3000;

bootstrap(port);
import { MongooseConfig } from './config/mongoose.config';
import { Module } from "@nestjs/common";
import { ConfigModule } from '@nestjs/config';
import { PostModule } from './post/post.module';
import { AuthModule } from './auth/auth.module';
import { CommentModule } from './comment/comment.module';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './user/user.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: ['.env'],
            isGlobal: true
        }),
        MongooseModule.forRoot(MongooseConfig.URI, MongooseConfig.OPTIONS),
        PostModule,
        AuthModule,
        CommentModule,
        UserModule
    ]
})
export class AppModule {}
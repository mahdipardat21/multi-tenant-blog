

export const MongooseConfig = {
    URI: 'mongodb://localhost/tenant-blog',
    OPTIONS: { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true }
}
import { Schema, Prop, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

@Schema({ timestamps: true })
export class User extends Document {

    @Prop({ required: true, trim: true })
    username: string;

    @Prop({ required: true, unique: true, trim: true })
    email: string;

    @Prop({ required: true, minlength: 8 })
    password: string;

}

export const userSchema = SchemaFactory.createForClass(User);